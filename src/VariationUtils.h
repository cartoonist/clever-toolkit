/* Copyright 2012 Tobias Marschall
 *
 * This file is part of CLEVER.
 *
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef VARIATIONUTILS_H
#define VARIATIONUTILS_H

#include <vector>

#include <boost/unordered_map.hpp>

#include "Variation.h"
#include "NamedDnaSequence.h"

class VariationUtils {
private:
	typedef struct {
		bool operator()(const Variation& v1, const Variation& v2) {
			if (v1.getType() != v2.getType()) return v1.getType() < v2.getType();
			if (v1.getChromosome() != v2.getChromosome()) return v1.getChromosome() < v2.getChromosome();
			int length1 = (v1.getType() == Variation::DELETION)?v1.getLengthDifference():-v1.getLengthDifference();
			int length2 = (v2.getType() == Variation::DELETION)?v2.getLengthDifference():-v2.getLengthDifference();
			if (length1 != length2) return length1 < length2;
			return v1.getCoordinate1() > v2.getCoordinate1();
		}
	} variation_sort_t;
public:
	typedef struct {
		bool operator()(const Variation& v1, const Variation& v2) {
			if (v1.getChromosome() != v2.getChromosome()) return v1.getChromosome() < v2.getChromosome();
			if (v1.getCoordinate1() != v2.getCoordinate1()) return v1.getCoordinate1() < v2.getCoordinate1();
			if (v1.getType() != v2.getType()) return v1.getType() < v2.getType();
			if (v1.getCoordinate2() != v2.getCoordinate2()) return v1.getCoordinate2() < v2.getCoordinate2();
			return v1.getSequence() < v2.getSequence();
		}
	} variation_position_sort_t;

	/** Returns a sub-list of the given list of variations where all redundant variations have been 
	 *  removed, that is, for each equivalence class of variations, only one (the left/rightmost) variation
	 *  is retained. Two variations are considered equivalent if applying them to the reference genome
	 *  yields the same donor genome.
	 */
	static std::unique_ptr<std::vector<Variation> > removeRedundant(const boost::unordered_map<std::string,NamedDnaSequence*>& references, const std::vector<Variation>& variations, bool rightmost = false);
};

#endif // VARIATIONUTILS_H
