/* Copyright 2012 Tobias Marschall
 *
 * This file is part of CLEVER.
 *
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef MISMATCHWEIGHTTRACK_H
#define MISMATCHWEIGHTTRACK_H

#include <vector>

#include <boost/unordered_map.hpp>

#include <bamtools/api/BamAlignment.h>

/** Class to sum up mismatch weights along the genome to find evidence for SNPs. 
 *  Uses 1 byte for each position. Internally, all weights are multiplied by 10 and truncated.
 */
class MismatchWeightTrack {
private:
	typedef std::vector<unsigned char> track_t;
	typedef boost::unordered_map<int,track_t*> all_tracks_t;
	// maps chromosome id to vector of accumulated weights
	all_tracks_t tracks;
public:
	typedef struct snp_t {
		int chromosome_id;
		unsigned int position;
		double weight;
		snp_t(int chromosome_id, unsigned int position, double weight) : chromosome_id(chromosome_id), position(position), weight(weight) {}
	} snp_t;
	typedef struct mismatch_weight_t {
		int chromosome_id;
		unsigned int position;
		unsigned char weight;
		mismatch_weight_t(int chromosome_id, unsigned int position, unsigned char weight) : chromosome_id(chromosome_id), position(position), weight(weight) {}
	} mismatch_weight_t;
	
	MismatchWeightTrack();
	virtual ~MismatchWeightTrack();

	static void extractFromAlignment(const BamTools::BamAlignment& aln, double alignment_weight, int phred_offset, std::vector<mismatch_weight_t>* target);

	void addAll(const std::vector<mismatch_weight_t>& mismatch_weights);

	std::unique_ptr<std::vector<snp_t> > getSnpCandidates(double weight_cutoff);
};

#endif // MISMATCHWEIGHTTRACK_H
