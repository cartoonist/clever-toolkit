/* Copyright 2012 Tobias Marschall
 *
 * This file is part of CLEVER.
 *
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAPPING_H_
#define MAPPING_H_

#include <memory>
#include <ostream>
#include <string>
#include <vector>
#include <bamtools/api/BamAlignment.h>

#include "../Variation.h"
#include "../NamedDnaSequence.h"

class Read;

/** Abstract base class of a mapping of one read to the reference. */
class Mapping {
protected:
	int phred_score;
	int mismatch_phred_score;
	double probability;
public:
	Mapping() : phred_score(-1), mismatch_phred_score(-1), probability(-1.0) {}
	Mapping(int phred_score, int mismatch_phred_score) : phred_score(phred_score), mismatch_phred_score(mismatch_phred_score), probability(-1.0) {}
	virtual ~Mapping() {}
	virtual BamTools::BamAlignment getBamAlignment(Read& parent, const std::string& name) const = 0;
	virtual int getRefId() const  = 0;
	virtual int getStartPosition() const = 0;
	virtual int getEndPosition() const = 0;
	virtual bool isReverse() const = 0;
	virtual int longestIndel() const = 0;
	virtual int getPhredScore() const { return phred_score; }
	/** Returns phred score that only counts mismatches. */
	virtual int getMismatchPhredScore() const { return mismatch_phred_score; }
	virtual double getProbability() const { return probability; }
	/** Returns a list of variations supported by this alignment. As class Variation stores the 
	  * chromosome name explicitly, a vector of chromosome names must be given. 
	  * May return a null pointer of no such variations exist.
	  */
	virtual std::unique_ptr<std::vector<Variation> > getPutativeVariations(const std::vector<NamedDnaSequence*>& reference_list, bool rightmost) const = 0;
	virtual void print(std::ostream& os) const = 0;
};

inline std::ostream& operator<<(std::ostream& os, const Mapping& mapping) {
	mapping.print(os);
	return os;
}

#endif /* MAPPING_H_ */
