/* Copyright 2012 Tobias Marschall
 *
 * This file is part of CLEVER.
 *
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPLITMAPPING_H_
#define SPLITMAPPING_H_

#include <ostream>
#include <string>
#include <vector>
#include <bamtools/api/BamAlignment.h>

#include "../SplitAligner.h"
#include "Mapping.h"

/** Split alignments resulting from one pair of anchors. */
class SplitMapping : public Mapping {
private:
	int left_anchor_idx;
	int right_anchor_idx;
	Read& parent;
	std::vector<SplitAligner::split_alignment_t>* alignments;
public:
	SplitMapping(Read& parent);
	SplitMapping(int left_anchor_idx, int right_anchor_idx, Read& parent, std::vector<SplitAligner::split_alignment_t>* alignments, int phred_score, int mismatch_phred_score);
	virtual ~SplitMapping();
	virtual BamTools::BamAlignment getBamAlignment(Read& parent, const std::string& name) const;
	virtual int getRefId() const;
	virtual int getStartPosition() const;
	virtual int getEndPosition() const;
	virtual bool isReverse() const;
	virtual int longestIndel() const;
	/** Returns a list containing only the "best" variation. Variation is canonified before being returned. */
	virtual std::unique_ptr<std::vector<Variation> > getPutativeVariations(const std::vector<NamedDnaSequence*>& reference_list, bool rightmost = false) const;
	virtual void print(std::ostream& os) const;
};

#endif /* SPLITMAPPING_H_ */
