/* Copyright 2012 Tobias Marschall
 *
 * This file is part of CLEVER.
 *
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WHOLEMAPPING_H_
#define WHOLEMAPPING_H_

#include <vector>

#include <bamtools/api/BamAlignment.h>
#include <bamtools/api/BamAux.h>

#include "Mapping.h"

/** Non-split alignment resulting from one anchor or given as input. */
class WholeMapping : public Mapping {
private:
	int ref_id;
	int start_position;
	int end_position;
	bool reverse;
	std::vector<BamTools::CigarOp> cigar;
public:
	WholeMapping(int ref_id, int start_position, int end_position, bool reverse, std::vector<BamTools::CigarOp> cigar, int phred_score, int mismatch_phred_score);
	virtual ~WholeMapping();
	virtual BamTools::BamAlignment getBamAlignment(Read& parent, const std::string& name) const;
	virtual int getRefId() const;
	virtual int getStartPosition() const;
	virtual int getEndPosition() const;
	virtual bool isReverse() const;
	virtual int longestIndel() const;
	virtual std::unique_ptr<std::vector<Variation> > getPutativeVariations(const std::vector<NamedDnaSequence*>& reference_list, bool rightmost) const;
	virtual void print(std::ostream& os) const;
};

#endif /* WHOLEMAPPING_H_ */
