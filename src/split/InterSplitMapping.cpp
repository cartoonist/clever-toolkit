/* Copyright 2013 Tobias Marschall
 *
 * This file is part of CLEVER.
 *
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sstream>
#include <math.h>

#include "Read.h"
#include "InterSplitMapping.h"
#include "../BamHelper.h"

using namespace std;

InterSplitMapping::InterSplitMapping(Read& parent) : references(0), anchor1_idx(-1), anchor1_isleft(false), anchor2_idx(-1), anchor2_isleft(false), parent(parent), alignments(0) {
}

InterSplitMapping::InterSplitMapping(const std::vector<NamedDnaSequence*>* references, int anchor1_idx, bool anchor1_isleft, int anchor2_idx, bool anchor2_isleft, Read& parent, std::vector<SplitAligner::interchrom_split_alignment_t>* alignments, int phred_score, int mismatch_phred_score) : Mapping(phred_score, mismatch_phred_score), references(references), anchor1_idx(anchor1_idx), anchor1_isleft(anchor1_isleft), anchor2_idx(anchor2_idx), anchor2_isleft(anchor2_isleft), parent(parent), alignments(alignments) {
	if ((alignments != 0) && (alignments->size() > 0)) {
		phred_score = alignments->at(0).phred_score;
		assert(phred_score >= 0);
		probability = pow(10.0, -((double)phred_score)/10.0);
	}
}

InterSplitMapping::~InterSplitMapping() {
	if (alignments != 0) delete alignments;
}

const Read::anchor_t& InterSplitMapping::get_anchor1() const {
	if (anchor1_isleft) {
		assert(anchor1_idx < (int)parent.leftAnchors().size());
		return parent.leftAnchors()[anchor1_idx];
	} else {
		assert(anchor1_idx < (int)parent.rightAnchors().size());
		return parent.rightAnchors()[anchor1_idx];
	}
}

const Read::anchor_t& InterSplitMapping::get_anchor2() const {
	if (anchor2_isleft) {
		assert(anchor2_idx < (int)parent.leftAnchors().size());
		return parent.leftAnchors()[anchor2_idx];
	} else {
		assert(anchor2_idx < (int)parent.rightAnchors().size());
		return parent.rightAnchors()[anchor2_idx];
	}
}

BamTools::BamAlignment InterSplitMapping::getBamAlignment(Read& parent, const string& name) const {
	assert(alignments != 0);
	assert(alignments->size() > 0);
	const SplitAligner::interchrom_split_alignment_t& split_aln = alignments->at(0);
	const Read::anchor_t& anchor1 = get_anchor1();
	const Read::anchor_t& anchor2 = get_anchor2();
	BamTools::BamAlignment a;
	a.Name = name;
	if (anchor1.alignment->type == SplitAligner::LEFT_ANCHOR) {
		a.Position = anchor1.position;
	} else {
		a.Position = split_aln.reference_breakpoint1;
	}
	a.RefID = anchor1.ref_id;
	if (anchor1.reverse) {
		a.SetIsReverseStrand(true);
		ShortDnaSequence rev = parent.getSequence().reverseComplement();
		a.QueryBases = rev.toString();
		a.Qualities = rev.qualityString();
	} else {
		a.SetIsReverseStrand(false);
		a.QueryBases = parent.getSequence().toString();
		a.Qualities = parent.getSequence().qualityString();
	}
	a.MapQuality = 255;
	a.CigarData = split_aln.cigar1;
	a.SetIsPaired(true);
	assert(phred_score >= 0);
	uint32_t as_tag = phred_score;
	if (!a.AddTag("AS", "I", as_tag)) assert(false);
	uint32_t ym_tag = mismatch_phred_score;
	if (!a.AddTag("YM", "I", ym_tag)) assert(false);
	assert(references != 0);
	assert(anchor2.ref_id < references->size());
	ostringstream ic_oss;
	ic_oss << references->at(anchor2.ref_id)->getName();
	if (anchor2.alignment->type == SplitAligner::LEFT_ANCHOR) {
		ic_oss << ',' << anchor2.position;
	} else {
		ic_oss << ',' << split_aln.reference_breakpoint2;
	}
	ic_oss << ',' << split_aln.cigar2;
	if (!a.AddTag("IC", "Z", ic_oss.str())) assert(false);
	ostringstream bp_oss;
	bp_oss << references->at(anchor1.ref_id)->getName() << ',' << split_aln.reference_breakpoint1 << ';';
	bp_oss << references->at(anchor2.ref_id)->getName() << ',' << split_aln.reference_breakpoint2;
	if (!a.AddTag("BP", "Z", bp_oss.str())) assert(false);
	uint32_t ye_tag = split_aln.split_cost;
	if (!a.AddTag("YE", "I",ye_tag)) assert(false);
	// TODO: Report alternative CIGAR strings?
	// include YA tag for alternative CIGAR strings
// 	if (alignments->size() > 1) {
// 		ostringstream oss;
// 		for (size_t i = 1; i < alignments->size(); ++i) {
// 			if (i>1) oss << ';';
// 			oss << alignments->at(i).phred_score << ',' << alignments->at(i).mismatch_phred_score << ',' << alignments->at(i).cigar;
// 		}
// 		if (!a.AddTag("YA", "Z", oss.str())) assert(false);
// 	}
	return a;
}

int InterSplitMapping::getRefId() const {
	return get_anchor1().ref_id;
}

int InterSplitMapping::getStartPosition() const {
	return get_anchor1().position;
}

int InterSplitMapping::getEndPosition() const {
	// TODO 
	return -1;
}

bool InterSplitMapping::isReverse() const {
	return get_anchor1().reverse;
}

int InterSplitMapping::longestIndel() const {
	return numeric_limits<int>::max();
}

unique_ptr<vector<Variation> > InterSplitMapping::getPutativeVariations(const std::vector<NamedDnaSequence*>& reference_list, bool rightmost) const {
	assert(alignments != 0);
	assert(alignments->size() > 0);
	unique_ptr<vector<Variation> > result(new vector<Variation>());
	return result;
}

void InterSplitMapping::print(std::ostream& os) const {
	os << getRefId() << ", " << getStartPosition() << " - " << getEndPosition() << ", PHRED: " << getPhredScore() << ", " << (isReverse()?"REVCOMP":"FORWARD") << ", INTERCHROMOSOMAL";
}
