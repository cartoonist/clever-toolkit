/* Copyright 2013 Tobias Marschall
 * 
 * This file is part of CLEVER.
 * 
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef VERSIONINFO_H
#define VERSIONINFO_H

#include <iostream>
#include <sstream>
#include <stdexcept>

#include <version.h>


class VersionInfo {
private:
	static std::string library_version;
public:
	inline static void checkAndPrintVersion(const std::string& program_name, std::ostream& os) {
		if (library_version.compare(VERSION) != 0) {
			std::ostringstream oss;
			oss << "Error: version mismatch! Library version: " << library_version << ". Executable version: " << VERSION;
			throw std::runtime_error(oss.str());
		}
		os << program_name << " version: " << VERSION << std::endl;
	}
	
	static std::string commandline(int argc, char* argv[]);

	static std::string version() { return VERSION; }
};

#endif // VERSIONINFO_H
