/* Copyright 2012 Tobias Marschall
 *
 * This file is part of CLEVER.
 *
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VARIATIONLISTPARSER_H_
#define VARIATIONLISTPARSER_H_

#include <iostream>
#include <vector>
#include <memory>

#include "Variation.h"

class VariationListParser {
private:
	VariationListParser() {}
	VariationListParser(const VariationListParser&) {}
public:
	/** @param weights If not zero, then the last column is expected to contain weights for each variant which are 
	 *                 written to the given vector. As a post-condition, the returned vector of variations has the
	 *                 same length as the weights vector.
	 */
	static std::unique_ptr<std::vector<Variation> > parse(std::istream& is, bool strip_chr_name = false, int min_length = -1, std::vector<double>* weights = 0, Variation::variation_type_t only_type = Variation::NONE);
};

#endif /* VARIATIONLISTPARSER_H_ */
