[![install with bioconda](https://img.shields.io/badge/install%20with-bioconda-brightgreen.svg?style=flat-square)](http://bioconda.github.io/recipes/clever-toolkit/README.html)

# CTK -- The CLEVER Toolkit
CTK is a suite of tools to analyze next-generation sequencing data
and, in particular, to discover and genotype insertions and deletions
from paired-end reads. The main software is written 
in C++ with some auxiliary scripts in Python. It is licensed
under the terms of the GNU General Public License Version 3, 
see file LICENSE.

# DOCUMENTATION
For documentation and installation instructions, visit our [Wiki](https://bitbucket.org/tobiasmarschall/clever-toolkit/wiki/Home)

# CONTACT
In order to get support, you can 

 1. subscribe to and ask questions at the CTK google group via
**clever-toolkit@googlegroups.com**
OR
[this website](https://groups.google.com/forum/?hl=en#!forum/clever-toolkit).

 2. post a bug report or feature request in our [issue tracker](https://bitbucket.org/tobiasmarschall/clever-toolkit/issues)

 3. contact me directly [Tobias Marschall](http://people.mpi-inf.mpg.de/~marschal/)