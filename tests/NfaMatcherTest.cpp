#include <boost/test/unit_test.hpp>
#include "../src/NfaMatcher.h"

using namespace std;

BOOST_AUTO_TEST_CASE( NfaMatcherTest ) {
	string s = "GTAACAGAT";
	NfaMatcher matcher(s, s.size());
	// NfaMatcher matcher("G");
	string text = "CACTAGTAACAGATAGAAGATCATGTAACAGATCGATTAGGTCGAGCTCATGCGTAACAGATCCAAAAACAAGTACCAGATTGCCGAGCGTAACAAGATGGAGAGGTA";
	unique_ptr<vector<pair<size_t,size_t> > > result = matcher.findMatches(text, 1);
	BOOST_CHECK_EQUAL(result->size(), 11);
	BOOST_CHECK_EQUAL(result->at(0).first, 12);
	BOOST_CHECK_EQUAL(result->at(0).second, 1);
	BOOST_CHECK_EQUAL(result->at(1).first, 13);
	BOOST_CHECK_EQUAL(result->at(1).second, 0);
	BOOST_CHECK_EQUAL(result->at(2).first, 14);
	BOOST_CHECK_EQUAL(result->at(2).second, 1);

	BOOST_CHECK_EQUAL(result->at(3).first, 31);
	BOOST_CHECK_EQUAL(result->at(3).second, 1);
	BOOST_CHECK_EQUAL(result->at(4).first, 32);
	BOOST_CHECK_EQUAL(result->at(4).second, 0);
	BOOST_CHECK_EQUAL(result->at(5).first, 33);
	BOOST_CHECK_EQUAL(result->at(5).second, 1);

	BOOST_CHECK_EQUAL(result->at(6).first, 60);
	BOOST_CHECK_EQUAL(result->at(6).second, 1);
	BOOST_CHECK_EQUAL(result->at(7).first, 61);
	BOOST_CHECK_EQUAL(result->at(7).second, 0);
	BOOST_CHECK_EQUAL(result->at(8).first, 62);
	BOOST_CHECK_EQUAL(result->at(8).second, 1);

	BOOST_CHECK_EQUAL(result->at(9).first, 80);
	BOOST_CHECK_EQUAL(result->at(9).second, 1);
	BOOST_CHECK_EQUAL(result->at(10).first, 98);
	BOOST_CHECK_EQUAL(result->at(10).second, 1);
}
