#include <boost/test/unit_test.hpp>
#include "../src/SplitAligner.h"
#include "../src/ShortDnaSequence.h"

using namespace std;

BOOST_AUTO_TEST_CASE( SplitAlignerTest ) {
	string ref    = "TGTCTGAATTGCAGCACTGCGCGTCTATGAGTTGGTGTCGGCAAAGCTGACAGAGATATTGGCGCAACCCCCTACTGCTGTTGCAAAATTGAGGCAAATA";
	ShortDnaSequence query1("GCTGCACTGCCGTCTTGCCGTCATGAGTCGTTAAATCGCCGGTAGACCAGGACTAGGC", "#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]");

	SplitAligner aligner(10, SplitAligner::LINEAR, 5);
	unique_ptr<SplitAligner::anchor_alignment_t> anchor = aligner.computeAnchorAlignment(ref, query1, 10, SplitAligner::LEFT_ANCHOR);
	BOOST_CHECK_EQUAL(anchor->column_count, 17);
}
